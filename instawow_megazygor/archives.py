import json
import subprocess
from contextlib import contextmanager
from pathlib import Path
from instawow.pkg_archives import (
    Archive,
    find_archive_addon_tocs,
    make_archive_member_filter_fn,
)

LSAR_PATH = 'lsar'
UNAR_PATH = 'unar'


@contextmanager
def open_rar_archive(path: Path):
    lsar_output = subprocess.check_output(
        [LSAR_PATH, "-j", "-jss", path],
    )
    lsar_json = json.loads(lsar_output)

    assert lsar_json["lsarFormatVersion"] == 2

    names = [m["XADFileName"] for m in lsar_json["lsarContents"]]
    top_level_folders = {h for _, h in find_archive_addon_tocs(names)}

    def extract(parent: Path):
        should_extract = make_archive_member_filter_fn(top_level_folders)
        subprocess.check_call(
            [
                UNAR_PATH,
                "-quiet",
                "-o",
                parent,
                path,
                *(n for n in names if should_extract(n)),
            ]
        )

    yield Archive(top_level_folders, extract)
