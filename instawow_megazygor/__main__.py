import instawow.plugins
from instawow.definitions import Defn, ChangelogFormat, SourceMetadata
from instawow.resolvers import BaseResolver, PkgCandidate
from instawow.results import PkgNonexistent

from .mediafire import get_zygor_download_url, get_zygor_version
from .archives import open_rar_archive


class MegaZygorResolver(BaseResolver):
    metadata = SourceMetadata(
        id='megazygor',
        name='Zygor from mediafire',
        strategies=frozenset(),
        changelog_format=ChangelogFormat.Raw,
        addon_toc_key=None,
    )
    requires_access_token = None
    archive_opener = staticmethod(open_rar_archive)

    async def _resolve_one(self, defn: Defn, metadata: None) -> PkgCandidate:
        if defn.alias != 'zygor':
            raise PkgNonexistent
        version, date_published = await get_zygor_version()
        return PkgCandidate(
            id='1',
            slug=defn.alias,
            name='ZygorGuidesViewer',
            description='Zygor Guides',
            url='https://zygorguides.com/',
            download_url=await get_zygor_download_url(),
            date_published=date_published,
            version=version,
            changelog_url='data:,',
        )


@instawow.plugins.hookimpl
def instawow_add_resolvers():
    return (MegaZygorResolver,)
