import aiohttp
import datetime
import asyncio
import re
from pathlib import Path

from mediafire.client import MediaFireClient


MF_DIR_URL = 'https://www.mediafire.com/folder/qy78iponp211t'
MF_DIR = 'mf:{}/'.format(MF_DIR_URL.rsplit('/')[-1])

MF_HEADERS = {
    'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36",
    'Accept-Encoding': "gzip",
}


async def get_mf_entry():
    client = MediaFireClient()
    entries = list(client.get_folder_contents_iter(MF_DIR))
    versions = []
    for entry in entries:
        m = re.search(r'Release +([\d\.]+)\.(rar|7z)', entry['filename'])
        if m is not None:
            versions.append((m[1], entry))
    versions.sort(reverse=True)
    if not versions:
        raise RuntimeError(
            "Cannot find a Zygor version in Mediafire dir listing"
        )
    return versions[0]


async def get_zygor_version():
    version, entry = await get_mf_entry()
    return version, datetime.datetime.fromisoformat(entry['created_utc'])


async def get_zygor_download_url():
    version, entry = await get_mf_entry()
    url = entry['links']['normal_download']
    async with aiohttp.ClientSession() as session:
        response = await session.get(url, headers=MF_HEADERS)
        body = await response.text()
    if m := re.search(r'href="((http|https)://download[^"]+)', body):
        return m.groups()[0]
    raise RuntimeError("Could not find a download URL in the Mediafire body.")


async def download_zygor():
    version, entry = await get_mf_entry()
    url = await get_zygor_download_url()
    async with aiohttp.ClientSession() as session:
        response = await session.get(url)
        return await response.read()


if __name__ == '__main__':
    out = asyncio.run(download_zygor())
    out_path = Path('./megazygor.out.rar')
    out_path.write_bytes(out)
    print(out_path)
