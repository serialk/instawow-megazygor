from setuptools import setup, find_packages


setup(
    name='instawow_megazygor',
    packages=find_packages(
        include=['instawow_megazygor', 'instawow_megazygor.*']),
    install_requires=['aiohttp', 'mediafire'],
    entry_points={
        'instawow.plugins': [
            'instawow_megazygor = instawow_megazygor.__main__'
        ]
    },
)
